var validadorController = function($, window, _){
    window.utils.establecerFondo("pantalla_inicio");
    
    $("#form_validar").submit(function(e) {
        e.preventDefault();
        var $valor = $("#txtCodigo").val();
        /*$.ajax({
            url: 'http://editorialescar.com/app/ver/' + $valor,
            dataType: 'jsonp',
            method: 'post',
            timeout: 5000
        });*/

        $.jsonp({
            url: 'http://editorialescar.com/app/ver/' + $valor,
            callbackParameter: 'retorno',
            callback: 'retorno',
            success: function(data, status) {
                if (data.error === 0 && data.valor === "OK"){
                    localStorage.setItem("escar.codigo", data.cd);
                    alert('Codigo aceptado');
                    selectorOpController($, window, _);
                }
                else{
                    alert('No se pudo validar el codigo- no valido jsonp');
                }
            },
            error: function(){
                alert('No se pudo validar el codigo- error jsonp');
            }
        });
    });

    $("#btnTrial").click(function(e) {
        e.preventDefault();

        $.jsonp({
            url: 'http://editorialescar.com/app/trial/registrar/' + device.uuid,
            callbackParameter: 'retorno',
            callback: 'retorno',
            success: function(data, status) {
                if (data.success === true){
                    alert('Periodo de prueba activado');
                    window.trial = "1";
                    selectorOpController($, window, _);
                }
                else{
                    alert(data.message);
                }
            },
            error: function(){
                alert('No se pudo validar el codigo- error jsonp');
            }
        });
    });
    
    window.retorno = function (res){
        /*if (res.error === 0 && res.valor === "OK"){
            localStorage.setItem("escar.codigo", res.cd);
            alert('Codigo aceptado');
            selectorOpController($, window, _);
        }
        else{
            alert('No se pudo validar el codigo-retorno');
        }*/
    };
};