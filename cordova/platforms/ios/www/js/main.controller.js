var mainController = function($, window, _){
    window.utils.establecerFondo("pantalla_inicio");
    
    var $codigo = localStorage.getItem("escar.codigo");
    
    window.ambiente = window.utils.reproducirSonido("ambiente", true);
    
    if (!_.isNull($codigo) && !_.isEmpty($codigo)){
        $("#inicio").load('./validador.html', function(res) {
            $("#lnkEmpezar").css('display', 'block');
            $("#lnkInstrucciones").css('display', 'block');
            $("#validacion").css('display', 'none');

            $("#lnkEmpezar").click(function(e) {
                e.preventDefault();
                selectorOpController($, window, _);
            });

            $("#lnkInstrucciones").click(function(e) {
                e.preventDefault();
                ayudaController($, window, _);
            });
        });
    }
    else{
		window.trial = "0";
		$.jsonp({
            url: 'http://editorialescar.com/app/trial/validar/' + device.uuid,
            callbackParameter: 'retorno',
            callback: 'retorno',
            success: function(data, status) {
                if (data.success === true){
					window.trial = "1";
					
                    $("#inicio").load('./validador.html', function(res) {
                        $("#lnkEmpezar").css('display', 'block');
                        $("#lnkInstrucciones").css('display', 'block');
                        $("#validacion").css('display', 'none');

                        $("#lnkEmpezar").click(function(e) {
                            e.preventDefault();
                            selectorOpController($, window, _);
                        });

                        $("#lnkInstrucciones").click(function(e) {
                            e.preventDefault();
                            ayudaController($, window, _);
                        });
                    });
                }
                else{
                    $("#lnkEmpezar").css('display', 'none');
                    $("#lnkInstrucciones").css('display', 'none');
                    $("#validacion").css('display', 'block');
                    $("#inicio").load('./validador.html', function(res) {
                        validadorController($, window, _);
                    });
                }
            },
            error: function(){
                
            }
        });
    }
    
    window.retorno = function (data){
        if (data.success === true){
            window.trial = "1";
            
            $("#inicio").load('./validador.html', function(res) {
                $("#lnkEmpezar").css('display', 'block');
                $("#lnkInstrucciones").css('display', 'block');
                $("#validacion").css('display', 'none');

                $("#lnkEmpezar").click(function(e) {
                    e.preventDefault();
                    selectorOpController($, window, _);
                });

                $("#lnkInstrucciones").click(function(e) {
                    e.preventDefault();
                    ayudaController($, window, _);
                });
            });
        }
        else{
            $("#lnkEmpezar").css('display', 'none');
            $("#lnkInstrucciones").css('display', 'none');
            $("#validacion").css('display', 'block');
            $("#inicio").load('./validador.html', function(res) {
                validadorController($, window, _);
            });
        }
    };
};