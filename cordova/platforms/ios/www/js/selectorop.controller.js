var selectorOpController = function($, window, _){
        
    $("#inicio").load('./selector_op.html', function(res) {
        window.utils.establecerFondo('fondo2');
		
		if (window.trial === "1"){
			$("#trResta").css("display", "none");
			$("#trMulti").css("display", "none");
			$("#trDiv").css("display", "none");
			$("#trFrac").css("display", "none");
			$("#tabla_operaciones").css("height", "auto");
		}
		
        $("#lnkInstrucciones").click(function(e) {
            e.preventDefault();
            ayudaController($, window, _);
        });
    
        $("a[rel='lnkOp']").click(function(e) {
            e.preventDefault();                            
            var $op = $(this).attr("data-op");
            $("#inicio").load('./tablero.html', function(res) {
                tableroController($, window, _, $op);
            });
        });
    });
};