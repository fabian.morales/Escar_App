/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        var mouseEventTypes = {
            touchstart : "mousedown",
            touchmove : "mousemove",
            touchend : "mouseup"
        };

        for (originalType in mouseEventTypes) {
            document.addEventListener(originalType, function(originalEvent) {
                event = document.createEvent("MouseEvents");
                touch = originalEvent.changedTouches[0];
                event.initMouseEvent(mouseEventTypes[originalEvent.type], true, true,
                        window, 0, touch.screenX, touch.screenY, touch.clientX,
                        touch.clientY, touch.ctrlKey, touch.altKey, touch.shiftKey,
                        touch.metaKey, 0, null);
                originalEvent.target.dispatchEvent(event);
            });
        }

        var contentWidth = document.body.scrollWidth,
            windowWidth = window.innerWidth,
            newScale = windowWidth / contentWidth;
        document.body.style.zoom = 0.5;

        (function($, window, _){
            $(document).ready(function() {
                $.ajaxSetup ({
                    // Disable caching of AJAX responses
                    cache: false
                });
                window.utils = utils();
                mainController($, window, _);
            });

            $(document).bind("mobileinit", function(){
               $.mobile.allowCrossDomainPages = true;
            });
        })(jQuery, window, _);
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        
    }
};

app.initialize();